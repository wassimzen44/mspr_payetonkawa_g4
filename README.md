# Projet MSPR - Solution Applicative pour PayeTonKawa

Ce projet MSPR (Mise en Situation Professionnelle Reconstituée) vise à moderniser le système d'information (SI) de la société fictive PayeTonKawa pour permettre la vente de café aux professionnels de la restauration. Le projet implique la conception et le développement d'une solution applicative basée sur une architecture micro-services et des API REST sécurisées.

## Objectif

Le principal objectif de ce projet est de développer une solution applicative fonctionnelle répondant aux besoins métiers de PayeTonKawa. Le projet est mené selon les méthodes agiles et implique la mise en œuvre de pratiques telles que la collecte des besoins métiers, la conception d'une architecture adaptée, le développement d'applications spécifiques, les tests de la solution, l'intégration continue et la conduite du changement.

## Technologies Utilisées

Les technologies utilisées dans ce projet sont les suivantes :

- Langage de Programmation : Java avec Spring Boot pour le backend.
- Base de Données : SQL Database.
- Frontend : HTML, CSS, Bootstrap.
- Outils de Développement : Docker pour le déploiement, utilisation de Git pour la gestion de version.

## Structure du Projet

Le projet est organisé en plusieurs parties :

1. **Back-end API** : Contient les API REST développées en Java avec Spring Boot.
2. **Base de Données** : Contient les schémas de base de données SQL utilisés par le système.
3. **Front-end Web** : Interface utilisateur développée en HTML, CSS et Bootstrap pour interagir avec les API.

## Installation et Exécution

Pour installer et exécuter ce projet sur votre propre machine, suivez ces étapes :

1. Clonez ce dépôt sur votre machine locale.
2. Assurez-vous d'avoir Java et Docker installés sur votre système.
3. Démarrez l'application en exécutant les commandes appropriées.
4. Accédez à l'interface utilisateur via votre navigateur web.


## Licence

Ce projet est sous licence [MIT](LICENSE).
